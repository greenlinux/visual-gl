#!/usr/bin/env bash

REMOTE_VERSION=$(curl -s https://gitlab.linuxmint.su/greenlinux/green-update/-/tags | grep -Eo "href=\"/greenlinux/green-update/-/tags/[0-9]+\.[0-9]+\.[0-9]" | cut -d '/' -f 6 | sort -rt . | head -n 1)
LOCAL_VERSION=$(grep "DISTRIB_RELEASE" /etc/lsb-release | cut -d "=" -f 2)

if [[ $REMOTE_VERSION > $LOCAL_VERSION ]]; then
    zenity --question --text="ВНИМАНИЕ! Перед обновлением рекомендуется создать резервную копию. После установки возможна смена темы, на стандартную. Вышла новая версия операционной системы $REMOTE_VERSION. Хотите установить?"
    case $? in
        0)
            zenity --info --text="Загрузка новой версии..."
            cd /tmp/
            wget -q "https://gitlab.linuxmint.su/greenlinux/green-update/-/archive/${REMOTE_VERSION}/green-update-${REMOTE_VERSION}.zip"
            unzip -qo "green-update-${REMOTE_VERSION}.zip"
            chmod +x "/tmp/green-update-${REMOTE_VERSION}/upgrade.sh"
            sudo cp -r "/tmp/green-update-${REMOTE_VERSION}/" "/var/greenlinux-upgrade/"
            zenity --info --text="Устанавливаем обновления..."
            sudo /var/greenlinux-upgrade/upgrade.sh
            cd /tmp && ls && sudo rm -rf * && cd ~ && sudo apt clean -y  && sudo apt install -f && sudo apt autoclean -y && sudo apt autoremove -y && sudo dpkg --configure -a
            cd ..
            zenity --info --text="Версия ${REMOTE_VERSION} установлена. Для применения обновлений Ваш компьютер будет перезагружен через 10 секунд."
            sleep 10
            sudo reboot
            ;;
        1)
            zenity --info --text="Вы отказались от обновлений."
            ;;
        -1)
            zenity --error --text="Произошла ошибка."
            ;;
    esac
else 
    zenity --info --text="Обновления отсутствуют."
fi
